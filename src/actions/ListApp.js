

export const SET_USER = 'SET_USER';
export function _setUser(user, token) {
    return {
        type: SET_USER,
        list: user.list,
        user: user,
        userToken: token
    }
}

export function setUser(user, token) {
    return dispatch => {
        return new Promise((resolve)=>{
            resolve(dispatch(_setUser(user, token)));
        });
    }
}

export const TOGGLE_SIGNED_IN = 'Toggle_Signed_In';
export function _toggleSignedIn(signedIn) {
    return {
        type: TOGGLE_SIGNED_IN,
        signedIn: signedIn
    }
}

export function toggleSignedIn(signedIn) {
    return dispatch => {
        return new Promise((resolve) => {
            resolve(dispatch(_toggleSignedIn(signedIn)));
        });
    }
}

export const REQUEST_USER_DATA = 'REQUEST_USER_DATA';
function requestUserData() {
    return {
        type: REQUEST_USER_DATA,
    }
}

export const RECEIVE_USER_DATA = 'RECEIVE_USER_DATA';
function receiveUserData(json, userToken) {
    return {
        type: RECEIVE_USER_DATA,
        user: json.data.user,
        list: json.data.user.list,
        userToken: userToken
    }
}

export const RECEIVE_LIST_DATA = 'RECEIVE_LIST_DATA';
function receiveListData(json) {
    return {
        type: RECEIVE_LIST_DATA,
        list: json.data.list,
    }
}

export const UPDATE_LIST_DATA = 'UPDATE_LIST_DATA';
function updateListData(newTaskValue, noteId) {
    return {
        type: UPDATE_LIST_DATA,
        newTaskValue: newTaskValue,
        noteId: noteId
    }
}

export const DELETE_NOTE = 'DELETE_NOTE';
function deleteNote(noteId) {
    return {
        type: DELETE_NOTE,
        noteId: noteId
    }
}

export const MARK_TASK_COMPLETE = 'MARK_TASK_COMPLETE';
function markTaskComplete(noteId) {
    return {
        type: MARK_TASK_COMPLETE,
        noteId: noteId
    }
}

export const REQUEST_ERROR = 'REQUEST_ERROR';
export function requestError(json) {
    return {
        type: REQUEST_ERROR,
        fetchErr: json.message
    }
}

export function getUserData() {
    return dispatch => {
        var userId = localStorage.getItem("userId");
        var userToken = localStorage.getItem("token");
        if (userId && userToken) {
            dispatch(requestUserData());
            return fetch(`http://localhost:8080/api/user/${userId}?token=${userToken}`)
                .then(response => response.json())
                .then(json => {
                    if (json.success) {
                        dispatch(receiveUserData(json, userToken));
                    } else {
                        dispatch(requestError(json));
                    }
                });
        }
    }
}

    
export function addTask(note, userId, userToken){
    return dispatch => {
        dispatch(requestUserData());
        fetch(`http://localhost:8080/api/task/${userId}?token=${userToken}`,
            {
                body: JSON.stringify(note),
                method: 'POST',
                headers: {'content-type': 'application/json'},
            })
            .then((res) => {
                if (res.ok) { return res.json(); }
                throw new Error('Network response was not ok.');
            })
            .then((json) => {
                if (json.success) {
                    dispatch(receiveListData(json));
                } else {
                    dispatch(requestError(json));
                    throw new Error(json.message);
                }
            }).catch((e) => {
                console.log(e);
            });
    }
}

export function updateTask (newTaskValue, userId, noteId, userToken) {
    return dispatch => {
        dispatch(requestUserData());
        fetch(`http://localhost:8080/api/task/${userId}/${noteId}?token=${userToken}`,
            {
                body: JSON.stringify(newTaskValue),
                method: 'Put',
                headers: {'content-type': 'application/json' },
            })
            .then((res) => {
                if (res.ok) { return res.json();}
                throw new Error('Network response was not ok.');
            })
            .then((json) => {
                if (json.success) { 
                    dispatch(updateListData(newTaskValue, noteId));
                } else {
                    dispatch(requestError(json));
                    throw new Error(json.message);
                }
            }).catch((e) => {
                console.log(e);
            });
    }
}

export function removeTask(noteId, userId, userToken) {
    return dispatch => {
        dispatch(requestUserData());
        fetch(`http://localhost:8080/api/task/${userId}/${noteId}?token=${userToken}`,
            {
                method: 'DELETE',
                headers: {'content-type': 'application/json'},
            })
            .then((res) => {
                if (res.ok) {return res.json();}
                throw new Error('Network response was not ok.');
            })
            .then((json) => {
                if (json.success) {
                    dispatch(deleteNote(noteId));
                } else {
                    dispatch(requestError(json));
                    throw new Error(json.message);
                }
            }).catch((e) => {
                console.log(e);
            });
    }
}

export function markComplete(note, userId, userToken) {
    var noteData = {
        ...note,
        complete: !note.complete,
    }
    return dispatch => {
        fetch(`http://localhost:8080/api/task/${userId}/${note._id}?token=${userToken}`,
            {
                body: JSON.stringify(noteData),
                method: 'Put',
                headers: { 'content-type': 'application/json' },
            })
            .then((res) => {
                if (res.ok) { return res.json(); }
                throw new Error('Network response was not ok.');
            })
            .then((json) => {
                if (json.success) {
                    dispatch(markTaskComplete(note._id));
                } else {
                    dispatch(requestError(json));
                    throw new Error(json.message);
                }
            }).catch((e) => {
                console.log(e);
            });
    }
}