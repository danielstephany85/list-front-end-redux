import {
    MARK_TASK_COMPLETE,
    DELETE_NOTE,
    SET_USER,
    TOGGLE_SIGNED_IN,
    REQUEST_USER_DATA,
    RECEIVE_LIST_DATA,
    RECEIVE_USER_DATA,
    UPDATE_LIST_DATA,
    REQUEST_ERROR,
} from "../actions/ListApp";

var initialState = {
    isFetching: false,
    signedIn: false,
    list: [],
    user: {},
    userToken: '',
}

export default function rootReducer(state = initialState, action) {
 
    switch (action.type) {
        case SET_USER:
            return {
                ...state,
                list: action.user.list,
                user: action.user,
                userToken: action.token,
                signedIn: true,
            };
        case TOGGLE_SIGNED_IN:
            return {
                ...state,
                signedIn: action.signedIn
            };
        case REQUEST_USER_DATA:
            return {
                ...state,
                isFetching: true
            };
        case RECEIVE_LIST_DATA:
            return {
                ...state,
                isFetching: false,
                list: action.list
            }
        case RECEIVE_USER_DATA:
            return {
                ...state,
                signedIn: true,
                isFetching: false,
                list: action.list,
                user: action.user,
                userToken: action.userToken
            };
        case UPDATE_LIST_DATA:
            return {
                ...state,
                isFetching: false,
                list: state.list.map((item)=>{
                    if(item._id === action.noteId){
                        return {
                            ...item,
                            ...action.newTaskValue
                        }
                    }
                    return item;
                })
            }
        case DELETE_NOTE:
            return {
                ...state,
                isFetching: false,
                list: state.list.filter((item) => {
                    if (item._id === action.noteId) { return false }
                    return true;
                })
            }
        case MARK_TASK_COMPLETE:
            return {
                ...state,
                isFetching: false,
                list: state.list.map((item) => {
                    if (item._id === action.noteId) {
                        return {
                            ...item,
                            complete: !item.complete,
                        }
                    }
                    return item;
                })
            }
        case REQUEST_ERROR:
            return {
                ...state,
                isFetching: false,
                fetchErr: true
            };
        default:
            return state;
    }
}
