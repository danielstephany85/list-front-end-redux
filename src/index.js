import React from 'react';
import ReactDOM from 'react-dom';
import './index.scss';
import ListApp from './ListApp';
import { Provider } from 'react-redux';
import { createLogger } from 'redux-logger'
import { createStore, applyMiddleware } from 'redux';
import ReduxThunk from 'redux-thunk';
import rootReducer from "./reducers/index";

//creating the redux store
const store = createStore(
    rootReducer,
    applyMiddleware(ReduxThunk),
    // window.__REDUX_DEVTOOLS_EXTENSION__ && window.__REDUX_DEVTOOLS_EXTENSION__()
);

ReactDOM.render(
    <Provider store={store} >
        <ListApp />
    </Provider>, document.getElementById('root'));
