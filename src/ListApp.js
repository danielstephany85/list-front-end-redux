import React, { Component } from 'react';
import { connect } from 'react-redux';
import { bindActionCreators } from 'redux';
import Header from './components/Header';
import ListContainer from './components/ListContainer/ListContainer';
import FooterNav from './components/FooterNav';
import NewNote from './components/NewNote';
import NotFound from './components/NotFound';
import Login from './components/Login';
import SignUp from './components/SignUp';
import Welcome from './components/Welcome';
import {
  BrowserRouter,
  Switch,
  Route,
  Redirect
} from 'react-router-dom';
import createBrowserHistory from 'history/createBrowserHistory';
import * as ListAppActions from "./actions/ListApp";


class ListApp extends Component {

  constructor(props){
    super(props);
    this.markComplete = bindActionCreators(ListAppActions.markComplete, this.props.dispatch);
    this.removeTask = bindActionCreators(ListAppActions.removeTask, this.props.dispatch);
    this.updateTask = bindActionCreators(ListAppActions.updateTask, this.props.dispatch);
    this.addTask = bindActionCreators(ListAppActions.addTask, this.props.dispatch);
    this.setUser = bindActionCreators(ListAppActions.setUser, this.props.dispatch);
    this.getUserData = bindActionCreators(ListAppActions.getUserData, this.props.dispatch);
    this.toggleSignedIn = bindActionCreators(ListAppActions.toggleSignedIn, this.props.dispatch);
    this.history = createBrowserHistory();
  }

  componentDidMount = () =>{
    this.getUserData();
  }

  setSignedIn = (cb) => {
    if (!this.props.signedIn){
      this.toggleSignedIn(!this.props.signedIn)
        .then(() => { if (typeof (cb) === "function") return cb();});
    }else{
      window.localStorage.setItem('userId', "");
      window.localStorage.setItem('token', "");
      this.toggleSignedIn(!this.props.signedIn)
        .then(() => { if (typeof (cb) === "function") return cb(); });
    }
  }

  navBack = (e) => {
    if(e) e.preventDefault();
    this.history.goBack();
  }

  render() {

    return (
      <BrowserRouter>
        <div>
          <Route path={'/'} render={(props) => <Header {...props} signedIn={this.props.signedIn} userName={this.props.signedIn ? this.props.user.name:''} /> } />
          <div className="main-content">
          <Switch>
              <Route exact path={'/'} render={(props) => 
                (!this.props.signedIn) ?
                  <Welcome {...props} />
                  :
                  <Redirect to="/list" />
              } />
            <Route exact path={'/login'} render={(props) => <Login {...props} setUser={this.setUser} />} />
            <Route exact path={'/sign-up'} render={(props) => <SignUp {...props} setUser={this.setUser} />} />
            <Route exact path={'/list'} render={(props) => 
              this.props.signedIn?
                  <ListContainer {...props} userId={this.props.user._id} userToken={this.props.userToken} signedIn={this.props.signedIn} list={this.props.list} removeTask={this.removeTask} markComplete={this.markComplete} toggleEdit={this.toggleEdit} />
                :
                <Redirect to="/"/>
            }/>
              <Route exact path={'/note'} render={(props) => <NewNote {...props} userId={this.props.user._id} userToken={this.props.userToken} name={this.props.user.name} signedIn={this.props.signedIn} list={this.props.list} updateTask={this.updateTask} addTask={this.addTask} removeTask={this.removeTask} markComplete={this.markComplete} />} />
              <Route exact path={'/note/:id'} render={(props) => <NewNote {...props} userId={this.props.user._id} userToken={this.props.userToken} name={this.props.user.name} signedIn={this.props.signedIn} list={this.props.list} updateTask={this.updateTask} addTask={this.addTask} removeTask={this.removeTask} markComplete={this.markComplete} />} />
            <Route path={'/not-found'} component={NotFound} />
          </Switch>
          </div>
          <Route path={'/'} render={(props) => <FooterNav {...props} setSignedIn={this.setSignedIn} signedIn={this.props.signedIn} navBack={this.navBack}/>} />
          </div>
      </BrowserRouter>
    );
  }
}

const mapStateToProps = state => (
  {
    isFetching: state.isFetching,
    signedIn: state.signedIn,
    list: state.list,
    user: state.user,
    userToken: state.userToken,
  }
);

export default connect(mapStateToProps)(ListApp);
