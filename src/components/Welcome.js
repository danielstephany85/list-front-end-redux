import React from 'react';
import { NavLink } from 'react-router-dom'

const Welcome = () => {

    return (
        <div className="content-section">
            <div className="text-block">
                <h1>Welcome To List App</h1>
                <p>
                    <NavLink to="/login" className="main-logo">login</NavLink> or <NavLink to="/signup" className="main-logo">signup</NavLink> to get started
                </p>
                
            </div>
        </div>
    );
}

export default Welcome;