import React, {Component} from 'react';

class SignUp extends Component {
    constructor(props) {
        super(props);
        this.state = {
            name: '',
            email: '',
            password: '',
            password2: '',
            passwordErr: false,
            password2Err: false,
            emailErr: false,
            nameErr: false
        }
    }

    checkPass = (password) => {
        var errorName = password + "Err"
        if (this.state[password] !== '') {
            if (this.state[password].length < 6) {
                this.setState({ [errorName]: "Password must be at least 6 charaters"});
                return false;
            }
        }else {
            this.setState({ [errorName]: "Password can not be empty"});
            return false;
        }
        return true;
    }

    matchPasswords = () => {
        var pass1Check = this.checkPass('password');
        var pass2Check = this.checkPass('password2');
        if (pass1Check && pass2Check){
            if (this.state.password !== this.state.password2) {
                this.setState({ 
                    passwork: '',
                    passwork2: '',
                    passwordErr: "The passwords dont match"
                 });
                return false;
            }
            return true;
        }
        return false;
    }

    checkEmail = () => {
        if (this.state.email === ''){
            this.setState({ emailErr: "Email can not be empty" });
            return false;
        }
        var emailCheck = /^(([^<>()\[\]\\.,;:\s@"]+(\.[^<>()\[\]\\.,;:\s@"]+)*)|(".+"))@((\[[0-9]{1,3}\.[0-9]{1,3}\.[0-9]{1,3}\.[0-9]{1,3}\])|(([a-zA-Z\-0-9]+\.)+[a-zA-Z]{2,}))$/;
        if(!emailCheck.test(this.state.email.toLowerCase())){
            this.setState({emailErr: "Please enter valid email" });
            return false;
        }
        return true;
    }


    handleSubmit = (e) => {
        e.preventDefault();
        this.setState({
            nameErr: '',
            passwordErr: '',
            password2Err: '',
            emailErr: '',
            responseErr: ''
        }, ()=>{
            var valid = true;
            if(this.state.name === ''){
                valid = false;
                this.setState({nameErr: "User name can not be empty."});
            }
            if (!this.matchPasswords()) valid = false;
            if (!this.checkEmail()) valid = false;
            // all data check out
            if (valid) {
                var user = {
                    name: this.state.name,
                    email: this.state.email,
                    password: this.state.password
                }
                
                fetch("http://localhost:8080/api/create-account",
                    { 
                        body: JSON.stringify(user),
                        method: 'POST',
                        headers: {
                            'content-type': 'application/json'
                        }, })
                    .then((response)=>{
                        if (response.ok){
                            return response.json();
                        }
                        throw new Error('Network response was not ok.');
                    })
                    .then((response) =>{ 
                        if(response.success){
                            this.props.setUser(response.data.user, response.data.token, ()=>{
                                window.localStorage.setItem('userId', response.data.user._id);
                                window.localStorage.setItem('token', response.data.token);
                                if (response.data.user.list.length > 0) {
                                    this.props.history.push('/list');
                                }else{
                                    this.props.history.push('/note');
                                }
                            });
                        }else {
                            throw new Error(response.message);
                        }
                    }).catch((e)=>{
                        this.setState({responseErr: e.message});
                    });
     
            // data format err
            } else {
                console.log('submit err');
                return;
            }
        });
       
    }



    render = () => {
        return (
            <div className="content-section">
                <div className="form-container">
                    <h2>Welcome to ListIt!</h2>
                    <h4>Create your accout to get started.</h4>
                    <form onSubmit={(e)=>{this.handleSubmit(e)}}>
                        <div className="form-container__item">
                            <label>
                                User Name
                                <input type="text" value={this.state.name} onChange={(e) => { this.setState({ name: e.target.value }); }} />
                            </label>
                            {this.state.nameErr ? <span className="error-msg">{this.state.nameErr}</span> : ""}
                        </div>
                        <div className="form-container__item">
                            <label>
                                Email
                                <input type="email" value={this.state.email} onChange={(e)=>{ this.setState({email: e.target.value}); }}/>
                            </label>
                            {this.state.emailErr? <span className="error-msg">{this.state.emailErr}</span>:""}
                        </div>
                        <div className="form-container__item">
                            <label>
                                Password
                                <input type="password" value={this.state.password} onChange={(e) => { this.setState({ password: e.target.value }); }}/>
                            </label>
                            {this.state.passwordErr ? <span className="error-msg">{this.state.passwordErr}</span> : ""}
                        </div>
                        <div className="form-container__item">
                            <label>
                                Re-enter Password
                                <input type="password" value={this.state.password2} onChange={(e) => { this.setState({ password2: e.target.value }); }}/>
                            </label>
                            {this.state.password2Err ? <span className="error-msg">{this.state.password2Err}</span> : ""}
                            {this.state.responseErr ? <span className="error-msg">{this.state.responseErr}</span> : ""}
                        </div>
                        <div className="form-container__item">
                            <button type="submit" className="form-btn">Sign Up</button>
                        </div>
                    </form>
                </div>
            </div>
        );
    }
}

export default SignUp;