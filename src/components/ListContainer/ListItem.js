import React, {Component} from 'react';
import PropTypes from 'prop-types';

class ListItem extends Component {

    render = () => {
        var complete = this.props.item.complete? "complete": "";

        return (
            <li className={`list-item ${complete}`}>
                <div>
                    <h3>{this.props.item.title}</h3>
                    <p>{this.props.item.body}</p>
                </div>  
                <button type="button" className="delete-btn" onClick={(e) => { e.preventDefault(); this.props.removeTask(this.props.item._id, this.props.userId, this.props.userToken) }}><i className="fas fa-trash"></i></button>
                <button type="button" className="check-btn" onClick={(e) => { e.preventDefault(); this.props.markComplete(this.props.item, this.props.userId, this.props.userToken) }}><i className="fas fa-check"></i></button>
            </li>
        );
    }
}

ListItem.propTypes = {
    item: PropTypes.object.isRequired,
    removeTask: PropTypes.func.isRequired,
    markComplete: PropTypes.func.isRequired,
    userId: PropTypes.string,
    userToken: PropTypes.string,
}

export default ListItem;