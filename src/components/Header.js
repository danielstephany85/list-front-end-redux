import React from 'react';
import { NavLink } from 'react-router-dom';

const Header = function(props) {
    var loginLinks = !props.signedIn ?
        <div>
            <NavLink className="nav-btn" to="/sign-up">Sign Up</NavLink>
            <NavLink className="nav-btn" to="/login">Login</NavLink>
        </div>
        :
        "";
        
    return (
        <header>
            <h1>Simple List</h1>
            <nav className="main-nav">
                {loginLinks}
                {props.userName}
            </nav>
        </header>
    );
}

export default Header;